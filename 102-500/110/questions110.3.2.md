Wie suche ich mit find nur nach Dateien/Ordner/symbolic Links?

find -type -f/-d/-l


Wie erzeugt man einen gpg Key?
In welchem Verzeichnis liegt er standardmäßig?

gpg --gen-key
~/.gnupg


Was beinhalten die folgenden GPG Dateien?
opengp-revocs.d
private-keys-v1.d
pubring.kbx
trustdb.gpg


opengp-revocs.d
Das Widerrufszertifikat, das zusammen mit dem Schlüsselpaar erstellt wurde. Die Berechtigungen für dieses Verzeichnis sind restriktiv, da jeder mit Zugriff auf das Zertifikat den Schlüssel widerrufen könnte.
private-keys-v1.d
Das Verzeichnis mit Ihrem privaten Schlüssel — daher die eingeschränkten Berechtigungen.
pubring.kbx
Ihr öffentlicher Schlüsselbund mit Ihrem eigenen sowie alle anderen importierten öffentlichen Schlüsseln.
trustdb.gpg
Die Vertrauensdatenbank — sie gehört zum Konzept des Web of Trust, das nicht Thema dieser Lektion ist.


Wie kann man sich den Inhalt des öffentlichen Schlüsselbunds ansehen?

gpg --list-keys


Was macht der folgende Befehl?
gpg --export carol > carol.pub.key

Er exportiert den öffentlichen Schlüssel von carol nach carol.pub.key.


Erkläre das Vorgehen mit GPG anhand einer Nachricht, die ich empfange?

Ich stelle meinen öffentlichen Schlüssel zur Verfügung, der Absenderschlüsselt damit die Nachricht und nur ich als Besitzer des privaten Schlüssels kann die Nachricht entschlüsseln.


Was machen gpg --keyserver keyserver-name --send-keys KEY-ID und gpg --keyserver keyserver-name --recv-keys KEY-ID?

Den eigenen öffentlichen Schlüssel auf dem Schlüsselserver hochladen und einen Key KEY-ID herunterladen.


Was macht gpg --output revocation_file.asc --gen-revoke KEY-ID?
Welche Aktion muss folgen um zu Widerrufen?

Ich zeige ein Widerrufszertifikat für KEY-ID an, welches ich in der Datei revocation_file.asc abspeichere.
Ich muss mit gpg --import revocation_file.asc das Zertifikat in den Schlüssel importieren um ihn zu widerrufen.


Wie importiere ich den Key carol.pub.key eines anderen Users?

Mit gpg --import carol.pub.key füge ich ihn meinem Schlüsselbund hinzu.


Erkläre den Befehl!
gpg --output encrypted-message --recipient carol --armor --encrypt unencrypted-message

--output encrypted-message
Angabe des Dateinamens für die verschlüsselte Version der Originaldatei (hier: encrypted-message).
--recipient carol
Angabe der USER-ID des Empfängers (hier: carol). Falls nicht angegeben, fragt GnuPG danach (es sei denn, --default-recipient ist definiert).
--armor
Erzeugt eine ASCII-basierende Ausgabe, die beispielsweise in eine E-Mail kopiert werden kann.
--encrypt unencrypted-message
Angabe des Dateinamens der zu verschlüsselnden Originaldatei (hier: unencrypted-message).

Wie entschlüsselt man encrypted-message?
Wie kann man die Entschlüsselte Version in unencrypted-message speichern?

gpg --decrypt encrypted-message
--output unencrypted-message


Mit welchem Befehl signiere ich message? Wie speichere ich das Ergebnis unter message.sig?

gpg --output message.sig --sign message


Wie prüfe ich die signierte Datei message.sig?

gpg --verify message.sig


Wie heißt ein Deamon der private GPG Schlüssel verwaltet?

gpg-agent


# Lösungen zu den geführten Übungen

Vervollständigen Sie die Tabelle durch Angabe des richtigen Dateinamens:
--
//- trustdb.gpg
//- opengp-revocs.d
//- private-keys-v1.d
//- pubring.kbx
--
Beschreibung 	    Dateiname
Vertrauensdatenbank    
Verzeichnis für Sperrzertifikate   
Verzeichnis für private Schlüssel
Öffentlicher Schlüsselring

Vertrauensdatenbank                 trustdb.gpg
Verzeichnis für Sperrzertifikate    opengp-revocs.d
Verzeichnis für private Schlüssel   private-keys-v1.d
Öffentlicher Schlüsselring          pubring.kbx

Bringen Sie die folgenden Schritte zum Widerruf des privaten Schlüssels in die richtige Reihenfolge:
--
//- Widerrufenen Schlüssel den Kommunikationspartnern zur Verfügung stellen
//- Sperrzertifikat erstellen
//- Sperrzertifikat in Schlüsselbund importieren
--

Die richtige Reihenfolge ist:
Schritt 1:  Sperrzertifikat erstellen
Schritt 2:  Sperrzertifikat in Schlüsselbund importieren
Schritt 3:  Widerrufenen Schlüssel den Kommunikationspartnern zur Verfügung stellen


Welche Art von Kryptographie nutzt GnuPG?

Kryptographie mit öffentlichem Schlüssel oder asymmetrische Kryptographie.


Welche sind die beiden Hauptbestandteile der Public-Key-Kryptographie?

Der öffentliche und der private Schlüssel.


Wie lautet die KEY-ID des Fingerabdrucks des öffentlichen Schlüssels 07A6 5898 2D3A F3DD 43E3 DA95 1F3F 3147 FA7F 54C7?

FA7F 54C7


Welche Methode wird für die Verteilung öffentlicher Schlüssel auf globaler Ebene verwendet?

Schlüsselserver


Was bedeutet die Option --armor in dem Befehl gpg --output encrypted-message --recipient carol --armor --encrypt unencrypted-message in Bezug auf die Dateiverschlüsselung?

Sie erzeugt eine geschützte ASCII-Ausgabe, so dass sich die resultierende verschlüsselte Datei in eine E-Mail kopieren lässt.
https://unix.stackexchange.com/questions/623375/what-is-the-armored-option-for-in-gnupg

gpg --export outputs binary data. In hexadecimal format, this looks like:
f53e9c4b013d3c6554c3161116face55f11db56dab1a941fe3a6e5ad246d4eb7
gpg --export --armor outputs base64 data, along with a plaintext header and footer:
-----BEGIN PGP PUBLIC KEY BLOCK-----
9T6cSwE9PGVUwxYRFvrOVfEdtW2rGpQf46blrSRtTrc=
-----END PGP PUBLIC KEY BLOCK-----
This is a more conventional format used when sharing keys in email and other text-oriented mediums. This is used because binary data can't be transmitted as ASCII text. Furthermore, base64 is much more compact than hexadecimal (i.e. base16).

# Lösungen zu den offenen Übungen

Die meisten gpg-Optionen haben sowohl eine lange als auch eine kurze Version. Vervollständigen Sie die Tabelle mit der entsprechenden Kurzversion:
Langfassung 	Kurzfassung
--armor
--output
--recipient
--decrypt
--encrypt
--sign

--armor     -a
--output    -o
--recipient -r
--decrypt   -d
--encrypt   -e
--sign      -s


Mit welchem Befehl exportieren Sie alle Ihre öffentlichen Schlüssel in eine Datei namens all.key?

gpg --export --output all.key
gpg --export -o all.key


Mit welchem Befehl exportieren Sie alle Ihre privaten Schlüssel in eine Datei namens all_private.key?

gpg --export-secret-keys --output all_private.key
gpg --export-secret-keys -o all_private.key


Mit welcher gpg-Option rufen Sie ein Menü auf, mit dem Sie die meisten Aufgaben im Zusammenhang mit der Schlüsselverwaltung durchführen?

--edit-key


Mit welcher gpg-Option erstellen Sie eine Klartextsignatur?

--clearsign