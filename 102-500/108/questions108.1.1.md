Auf welche Zeit sind Computer in der Regel eingestellt? Wie gibt man diese mit date aus?

Die Ortszeit berechnet sich anhand der eingestellten UTC, date -u


Wie zeigt man sich mit date die aktuelle Zeit im Unix Format an?

date +%s


Wie nutze ich date um mir die Unix Time 1564013011 lesbar anzuzeigen?

date --date='@1564013011'


Wie frage ich die Zeit der Echtzeituhr ab? Was ist außerdem notwendig hierfür?

sudo hwclock


Wie setze ich die Zeit mit timedatectl auf 14:00 am 25.11.2011 wenn NTP nicht verfügbar ist?

timedatectl set-time '2011-11-25 14:00:00'


Wie liste ich mit timedatectl alle Zeitzonen auf und wähle eine davon aus?

timedatectl list-timezones
timedatectl set-timezone Africa/Cairo


Wie deaktiviere ich NTP mit timedatectl?

timedatectl set-ntp no


Wie lege ich einen logischen Link für /etc/localtime auf einem deutschen System an?

ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime


Wie synchronisiert man die Hardware Uhr auf die Systemuhr?

hwclock --systohc


# Lösungen zu den geführten Übungen

Geben Sie an, ob die folgenden Befehle die Systemzeit oder die Hardware-Zeit anzeigen oder ändern:
Befehl(e) 	System 	Hardware 	Beide
date -u
hwclock --set --date "12:00:00"
timedatectl

System 	Hardware 	Beide
X
        X
                    X


Geben Sie an, ob die folgenden Befehle die Systemzeit oder die Hardware-Zeit anzeigen oder ändern:
Befehl(e) 	System 	Hardware 	Beide
timedatectl | grep RTC
hwclock --hctosys
  
System 	Hardware 	Beide
        X
X


Geben Sie an, ob die folgenden Befehle die Systemzeit oder die Hardware-Zeit anzeigen oder ändern:
Befehl(e) 	System 	Hardware 	Beide
date +%T -s "08:00:00"
timedatectl set-time 1980-01-10

    
System 	Hardware 	Beide
X
                    X


Betrachten Sie die folgende Ausgabe und korrigieren Sie das Format des Arguments so, dass der Befehl erfolgreich ausgeführt wird:
$ date --debug --date "20/20/12 0:10 -3"
date: warning: value 20 has less than 4 digits. Assuming MM/DD/YY[YY]
date: parsed date part: (Y-M-D) 0002-20-20
date: parsed time part: 00:10:00 UTC-03
date: input timezone: parsed date/time string (-03)
date: using specified time as starting value: '00:10:00'
date: error: invalid date/time value:
date:     user provided time: '(Y-M-D) 0002-20-20 00:10:00 TZ=-03'
date:        normalized time: '(Y-M-D) 0003-08-20 00:10:00 TZ=-03'
date:                                  ---- --
date:      possible reasons:
date:        numeric values overflow;
date:        incorrect timezone
date: invalid date ‘20/20/2 0:10 -3’

date --debug --set "12/20/20 0:10 -3"


Nutzen Sie nun hwclock, um die Hardware-Uhr nach der Systemuhr zu stellen.

hwclock -systohc


Es gibt einen Ort namens eucla. Zu welchem Kontinent gehört er? Verwenden Sie nur grep, um das herauszufinden, sowie timedatectl und grep. Geben Sie den vollständigen Befehl an.

timedatectl list-timezones | grep -i eucla
oder
grep -rni eucla /usr/share/zoneinfo


Setzen Sie Ihre aktuelle Zeitzone auf die von eucla. Gebe einen Befehl für timedatectl an und einen ohne timedatectl!

timedatectl set-timezone 'Australia/Eucla'
oder
ln -s /usr/share/zoneinfo/Australia/Eucla /etc/localtime


# Lösungen zu den offenen Übungen

Welche ist die beste Methode zum Einstellen der Zeit? In welchem Szenario ist die bevorzugte Methode nicht möglich?

In den meisten Linux-Distributionen ist NTP standardmäßig aktiviert und sollte so belassen werden, dass es die Systemzeit ohne Störung setzt. Ist das Linux-System jedoch nicht mit dem Internet verbunden, ist NTP nicht erreichbar. Ein Embedded Linux-System in einer Industrieanlage könnte ein solches System ohne Netzwerkverbindung sein.


Warum gibt es Ihrer Meinung nach so viele Methoden, um die Systemzeit zu setzen?

Da das Setzen der Zeit seit Jahrzehnten eine Voraussetzung für alle Unix-artigen Systeme ist, gibt es viele veraltete Methoden zum Setzen der Zeit, die immer noch gepflegt werden.


Nach dem 19. Januar 2038 wird die Linux-Systemzeit eine 64-Bit-Zahl zum Speichern benötigen. Es ist jedoch auch möglich, eine “Neue Epoche” zu setzen. Zum Beispiel könnte der 1. Januar 2038 um Mitternacht auf die “New Epoch Time” 0 gesetzt werden. Warum wäre das nicht die bevorzugte Lösung?

Im Jahr 2038 wird die überwiegende Mehrheit der Computer bereits mit 64-Bit-CPUs arbeiten und eine 64-Bit-Nummer wird die Leistung in keiner Weise beeinträchtigen. Es wäre jedoch unmöglich, die Risiken des “Zurücksetzens” der Epochenzeit auf diese Weise abzuschätzen. Es gibt zahlreiche Legacy-Software, die davon betroffen sein könnte. Banken und große Unternehmen haben zum Beispiel oft viele ältere Programme, auf die sie intern angewiesen sind. Das Szenario ist also, wie so viele andere, eine Studie über Kompromisse. Alle 32-Bit-Systeme, die im Jahr 2038 noch laufen, wären von einem Epochenzeit-Überlauf betroffen, aber Legacy-Software würde durch die Änderung des Epochenwerts beeinträchtigt.