Über welches Protokoll läuft die Übermittlung von Mails? Welcher Port?

SMTP 25


Wie heißt die Konfigurationsdatei für sendmail?

/etc/mail/sendmail.mc


Wie kann ich mit ncat eine Nachricht an den MTA auf lab2.campus senden?

nc lab2.campus 25


Wie kann man sich als root alle nicht zugestellten Mails anzeigen lassen?

mailq


Was ist ein MUA? Nenne Beispiele!

Mail User Agent, Thunderbird oder auch Webinterface Mailanbieter


Wie sende ich eine Mail mit dem Betreff Maintenance fail an henry@lab3.campus mit dem Inhalt "The maintenance script failed at `date`"?
Welche 3 anderen Methoden gibt es noch, einen Nachrichtentext mitzusenden?

$ mail -s "Maintenance fail" henry@lab3.campus <<<"The maintenance script failed at `date`"
Ebenfalls kann man den Inhalt einer Datei oder den Output eines Commands als Nachrichtentext senden.
Wird kein Nachrichtentext eingegeben, wartet mail auf die Eingabe des Nutzers und endet mit Strg D.


In welcher Datei wird der Mail Routing Mechanismus, also carol@lab2.campus ist der User carol, konfiguriert?

/etc/aliases


Was mus ich tun, um postmaster@lab2.campus an den User carol zuzustellen?

postmaster: carol in /etc/aliases aufnehmen und die Änderungen mit
newaliases wirksam machen.


Was muss ich tun um subscribe@lab2.campus an die Standardeingabe von subscribe.sh zuzustellen?

$ subscribe: |subscribe.sh


# Lösungen zu den geführten Übungen

Ohne weitere Optionen oder Argumente wechselt der Befehl mail henry@lab3.campus in den Eingabemodus, so dass der Benutzer eine Nachricht an henry@lab3.campus schreiben kann. Welcher Tastendruck schließt den Eingabemodus nach Beendigung der Nachricht und versendet die E-Mail?

Strg+D schließt das Programm und versendet die E-Mail.


Welchen Befehl kann der Root-Benutzer ausführen, um die nicht zugestellten Nachrichten aufzulisten, die auf dem lokalen System entstanden sind?

Der Befehl mailq oder sendmail -bp.


Wie nutzt ein unprivilegierter Benutzer die Standard-MTA-Methode, um alle eingehenden E-Mails automatisch an die Adresse dave@lab2.campus weiterzuleiten?

Er fügt dave@lab2.campus zu ~/.forward hinzu.


# Lösungen zu den offenen Übungen

Welcher Befehl sendet unter Verwendung des von mailx bereitgestellten mail-Befehls eine Nachricht an emma@lab1.campus mit der Datei logs.tar.gz als Anhang und der Ausgabe des Befehls uname -a als E-Mail-Text?

uname -a | mail -a logs.tar.gz emma@lab1.campus


Der Administrator eines E-Mail-Dienstes möchte den E-Mail-Verkehr im Netz überwachen, aber seine Mailbox nicht mit Testnachrichten überfrachten. Wie könnte er einen systemweiten E-Mail-Alias konfigurieren, um alle an den Benutzer test gesendeten E-Mails in die Datei /dev/null umzuleiten?

Die Zeile test: /dev/null in /etc/aliases leitet alle an die lokale Mailbox test gesendeten Nachrichten in die Datei /dev/null um.
Danach muss noch newaliases ausgeführt werden.