Wie öffnet man die Man Page zum Routing Table Management? Also einer Unterseite von ip?

man ip-route


Aus welchen Teilen besteht ein IPv4/6 Adresse?
Wodurch kann man die Teile erkennen?

Netzanteil | Hostanteil
Durch die Subnetzmaske werden die Teile angegeben.


Über welches Paket gelangt man in neueren Distributionen in der Regel an das alte Programm ifconfig?

net-tools


Wie listet man mit ip die verfügbaren Schnittstellen auf?

ip link


Wie listet man mit ip eine Gesamtübersicht über alle Schnittstellen auf?
Welche Befehle gibt es hierfür?

ip addr, ip a oder ip address


Wie kann ich mir nur mit ls alle Netzwerk Schnittstellen auflisten?

ls /sys/class/net


Wie füge ich mit ip dem Gerät enp0s8 die Adresse 192.168.5.5/24 zu?
Wie für 2001:db8::10/64 unter IPv6?

# ip addr add 192.168.5.5/24 dev enp0s8
# ip addr add 2001:db8::10/64 dev enp0s8


Wie deaktiviere ich die Schnittstelle enp0s8 mit ip?

# ip link set dev enp0s8 down
Dies ist eine Low-Level-Optionen. Daher verwendet man link.


Was ist die MTU?

Maximum Transmission Unit
Die Maximum Transmission Unit beschreibt die maximale Paketgröße eines Protokolls der Vermittlungsschicht (Schicht 3) des OSI-Modells, gemessen in Bytes, welche ohne Fragmentierung in den Rahmen eines Netzes der Sicherungsschicht (Schicht 2) übertragen werden kann. 

Wie ändert man mit ip die MTU von enp0s8 auf 2000?

# ip link set enp0s8 mtu 2000
Dies ist eine Low-Level-Optionen. Daher verwendet man link.


Mit welchen 3 Befehlen kann ich mir die Routing Tabelle anzeigen lasssen?
Wie für IPv6?

route, netstat -r und ip route
route -6, netstat -6r und ip -6 route


Erkläre die ersten drei Spalten!
$ route -6
Kernel IPv6 routing table
Destination                    Next Hop                   Flag Met  Ref Use  If
2001:db8::/64                  [::]                       U    256  0      0 enp0s8
2002:a00::/24                  [::]                       !n   1024 0      0 lo
[::]/0                         2001:db8::1                UG   1    0      0 enp0s8
localhost/128                  [::]                       Un   0    2     84 lo
ff00::/8                       [::]                       U    256  1      3 enp0s3

Zieladresse
Nächster Hop um die Destination zu errreichen
Flag:
  U = aktiv
  ! = nicht verwendet
  n = nicht gecached (der Kernel hält ein paar Routen im Cache vor, um schneller auf sie zuzugrifen. Aber nicht alle!
  G = Gateway


Erkläre die letzten vier Spalten!
$ route -6
Kernel IPv6 routing table
Destination                    Next Hop                   Flag Met  Ref Use  If
2001:db8::/64                  [::]                       U    256  0      0 enp0s8
2002:a00::/24                  [::]                       !n   1024 0      0 lo
[::]/0                         2001:db8::1                UG   1    0      0 enp0s8
localhost/128                  [::]                       Un   0    2     84 lo
ff00::/8                       [::]                       U    256  1      3 enp0s3

Metric:     Im Netzwerkbereich definiert die Metrik ein numerisches Maß für die Güte einer Verbindung bei Verwendung einer bestimmten Route.
Ref:        Reference, Verweise auf die Route
Use:        Die Anzahl der Suchvorgänge für eine Route.
If:         Interface



default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
Erkläre was default, via und dev bedeuten!

default
Das Ziel ist die Standardroute.
via
Die Gateway-Adresse lautet 10.0.2.2.
dev
Ist über die Schnittstelle enp0s3 erreichbar.


default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
Erkläre was proto, scope, metric und pref bedeuten!

proto
Es wurde per DHCP zur Routingtabelle hinzugefügt.
scope
Der Geltungsbereich ist nicht angegeben, so dass er global gilt.
metric
Die Route hat einen Aufwandswert von 100.
pref
Keine IPv6-Routenpräferenz.


fc0::/64 dev enp0s8 proto kernel metric 256 pref medium
Erkläre was default, dev und kernel bedeuten!

default
Das Ziel ist fc0::/64.
dev
Es ist über die Schnittstelle enp0s8 erreichbar.
kernel
Es wurde vom Kernel automatisch hinzugefügt.


fc0::/64 dev enp0s8 proto kernel metric 256 pref medium
Erkläre was scope, metric und pref bedeuten!

scope
Der Geltungsbereich ist nicht angegeben, so dass er global gilt.
metric
Die Route hat einen Aufwandswert von 256.
pref
Es hat eine IPv6-Präferenz von medium.


Wie pingt man die Adresse 2001:db8:1::20 an und stoppt nach 2 Antworten? 

# ping6 -c 2 2001:db8:1::20
Was macht dieser Command?

Er pingt die Adresse 2001:db8:1::20 an und stoppt nach 2 Antworten.


Wie füge ich mit route die Adresse 2001:db8:1::/64, erreichbar über das Gateway 2001:db8::3 hinzu?

# route -6 add 2001:db8:1::/64 gw 2001:db8::3
Was macht der Command?

Er fügt mit route die IPv6 Adresse 2001:db8:1::/64 welche erreichbar ist über das Gateway 2001:db8::3 hinzu.


Wie füge ich mit ip die Adresse 2001:db8:1::/64, erreichbar über das Gateway 2001:db8::3 hinzu?

# ip route add 2001:db8:1::/64 via 2001:db8::3
Was macht der Command?

Er fügt mit ip die IPv6 Adresse 2001:db8:1::/64 welche erreichbar ist über das Gateway 2001:db8::3 hinzu.


# Lösungen zu den geführten Übungen

Mit welchen Befehlen listen Sie die Netzwerkschnittstellen auf?

ip link (show), ifconfig -a oder ls /sys/class/net


Wie deaktivieren Sie eine Schnittstelle vorübergehend? Und wie aktivieren Sie diese wieder?

Mit ifconfig:
$ ifconfig wlan1 down
$ ifconfig wlan1 up
Mit ip link:
$ ip link set wlan1 down
$ ip link set wlan1 up


Welche der folgenden ist eine sinnvolle Subnetzmaske für IPv4?
--
//- 0.0.0.255
//- 255.0.255.0
//- 255.252.0.0
//- /24
--

- 255.252.0.0
- /24
Die anderen aufgelisteten Masken sind ungültig, weil sie die Adresse nicht sauber in zwei Abschnitte unterteilen, wobei der erste Teil das Netzwerk und der zweite den Host definiert. Die linken Bits einer Maske sind immer 1, die rechten Bits sind immer 0.


Mit welchem Befehl überprüfen Sie Ihre Standardroute?

Mit route, netstat -r oder ip route:
$ route
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
default         server          0.0.0.0         UG    600    0        0 wlan1
192.168.1.0     0.0.0.0         255.255.255.0   U     600    0        0 wlan1
$ netstat -r
Kernel IP routing table
Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
default         server          0.0.0.0         UG        0 0          0 wlan1
192.168.1.0     0.0.0.0         255.255.255.0   U         0 0          0 wlan1
$ ip route
default via 192.168.1.20 dev wlan1 proto static metric 600
192.168.1.0/24 dev wlan1 proto kernel scope link src 192.168.1.24 metric 600


Wie fügen Sie mit ip address einer Schnittstelle enp0s9 eine zweite IP-Adresse 172.16.15.16/16 hinzu?
Mit welchem Programm geht dies ebenfalls?

$ ip addr add 172.16.15.16/16 dev enp0s9 label enp0s9:sub1
Da ifconfig veraltet ist, lerne ich die Syntax net.


# Lösungen zu den offenen Übungen

Mit welchem Unterbefehl von ip konfigurieren Sie VLAN Tagging?

ip link hat eine Option vlan.

Hier ein Beispiel für das Taggen einer Subschnittstelle mit der VLAN-ID 20:
# ip link add link enp0s9 name enp0s9.20 type vlan id 20


Wie konfigurieren Sie eine Standardroute?

Mit route oder ip route:

# route add default gw 192.168.1.1
# ip route add default via 192.168.1.1


Wie erhalten Sie detaillierte Informationen über den Befehl ip neighbour? Was passiert, wenn Sie den Befehl selbst ausführen?

$ man ip-neigbour
Die Ausgabe zeigt Ihren ARP-Cache an:
$ ip neighbour
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 REACHABLE


Wie sichern Sie Ihre Routingtabelle? Wie stellen Sie sie wieder her?

Das folgende Beispiel zeigt Sicherung und Wiederherstellung einer Routingtabelle:
# ip route save > /root/routes/route_backup
# ip route restore < /root/routes/route_backup