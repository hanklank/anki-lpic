Welcher Befehl ist in der Grundausstattung jeder Distro enthalten und dient dazu, sich die Netzwerkschnittstellen anzuzeigen?
Womit kann ich dies auch erreichen, wenn die Distro über nmcli verfügt?

ip link show
nmcli device


So gut wie jedes Gerät verfügt über mind. 2 Netzwerkschnittstellen. Welche?

Die virtuelle Loopback-Schnittstelle und 
mindestens eine, die der vom System gefundenen Netzwerkhardware zugeordnet ist


# 3

Nach dem Bennenungsschema von systemd, welche Schnittstellentypen sind gemeint?
en
wl
ww

Ethernet
WLAN
WWAN (Wireless WAN)
Welche Kürzel haben die Schnittstellen nach dem Bennenungschema von Systemd?

en
wl
ww


Welche Namensschemen folgen die Geräte?
eno1
ens1
enp3s5
enx78e7d1ea46da
eth0

Nach dem vom BIOS oder von der Firmware des verbauten Gerätes bereitgestellten Index
Nach dem Index des PCI-Expresssteckplatzes, wie er vom BIOS oder der Firmware angegeben wird
Nach ihrer Adresse am entsprechenden Bus
Nach der MAC-Adresse der Schnittstelle
Nach der alten Konvention
Wie würden Gerätenamen des jeweiligen Namensschemas lauten?

eno1
ens1
enp3s5
enx78e7d1ea46da
eth0


Wie heißen die Hilfsbefehle für ip, mit denen man die in der Datei /etc/network/interfaces aufgeführten Schnittstellen verwaltet?

ifup ifdown
Was machen die Befehle? Von welchem Befehl stammen sie?

Die in /etc/network/interfaces aufgeführten Schnittstellen verwalten.


Wie startet man alle physikalischen Schnittstellen die mit auto beginnen in /etc/network/interfaces?

ifup -a
Was macht der Befehl?

Alle Schnittstellen, die mit auto beginnen in /etc/network/interfaces, starten.


Welchen Namen hätte das folgende Gerät?
$ lspci | fgrep Ethernet
03:05.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL-8110SC/8169SC Gigabit Ethernet (rev 10)

enp3s5
en = Ethernet
p3 = PCI Bus
s5 = Slot


Wie heißt ein alter Befehl zum Management der Netzwerkschnittstellen, der vor allem während des Boot Vorgangs von viel genutzt wird um die Netzwerkadapter zu konfigurieren und wie heißt sein Nachfolger?

Alt: ipconfig
Neu: ip


Wie sieht die Konfiguration in /etc/network/interfaces für loopback und enp3s5 aus?

auto lo
iface lo inet loopback
auto enp3s5
iface enp3s5 inet dhcp


Wie aktiviert und deaktiviert man diese Schnittstelle?
auto enp3s5
iface enp3s5 inet dhcp

ifup enp3s5, ifdown enp3s5


auto enp3s5
iface enp3s5 inet dhcp
Wie konfiguriert man diese Schnittstelle ohne DHCP mit der IP 192.168.1.2/24 und dem Gateway 192.168.1.1?

iface enp3s5 inet static
    address 192.168.1.2/24
    gateway 192.168.1.1


Was passiert, wenn mehrere iface Einträge angegeben sind?
iface enp3s5 inet static
    address 192.168.1.2/24
iface enp3s5 inet static
    gateway 192.168.1.1

Alle Optionen werden aufgerufen, wenn enp3s5 aufgerufen wird.


Wie setze ich den Hostnamen storage per Command?

hostnamectl set-hostname storage


Wie setze ich den Hostnamen storage manuell?

/etc/hostname bearbeiten


Wie kann ich einen hübschen Hostname setzen?

# hostnamectl --pretty set-hostname "LAN Shared Storage"


Wie kann ich den Transienten Hostnamen setzen?

# hostnamectl --transient set-hostname generic-host
Wozu dient er?

Wenn kein statischer Hostname gesetzt ist, oder dieser localhost lautet, wird der transiente Hostname verwendet.


Wie kann ich mir den Hostnamen, sowie den hübschen und transienten Hostnamen anzeigen lassen?

hostnamectl, oder hostnamectl status


Was wird hinsichtlich der DNS Auflösung in /etc/nsswitch.conf konfiguriert?

Welcher Suchprozess für DNS Namen Vorrang hat. Standard:
hosts: files dns, also /etc/hosts vor der Antwort des DNS Servers.


Wie und wo konfiguriert man den Vorrang von Dateien gegeüber den Antworten der DNS Server? Also der Standard?

/etc/nsswitch.conf
hosts: files dns


Welchen Eintrag enthält /etc/hosts standardmäßig? Wie heißt dieser für IPv6?

127.0.0.1 localhost
::1 localhost


Wie ordnet man dem Hostnamen foo.mydomain.org und foo die Domain 192.168.1.1 zu?

192.168.1.1 foo.mydomain.org foo


Mit welchem Eintrag legt man den DNS Server von Google 8.8.8.8 als Standard für DNS Abfragen fest? Wo?

/etc/resolv.conf
nameserver 8.8.8.8


Was bewirkt search mydomain.net mydomain.com in /etc/resolv.conf? Wonach wird bei somehost zusätzlich gesucht?

Wie domain, aber search akzeptiert eine Liste von Domainnamen, die durchsucht werden wenn in der Anfrage keine FQDN enthalten ist.
somehost.example.com and somehost.local.test
https://superuser.com/questions/570082/in-etc-resolv-conf-what-exactly-does-the-search-configuration-option-do


Was bewirkt domain mydomain.org in /etc/resolv.conf?

Akzeptiert einen Domainnamen, der ersetzt wird wenn in der Anfrage keine FQDN enthalten ist.
ping somehost wird als ping somehost.example.com ausgeführt.


# Lösungen zu den geführten Übungen

Mit welchen Befehlen listen Sie die Netzwerk-Schnittstellen im System auf?

Die Befehle ip link show, nmcli device und das alte ifconfig.


Von welchem Typ ist eine Netzwerk-Schnittstelle mit dem Namen wlo1?

Der Name beginnt mit wl, es handelt sich also um eine WLAN-Schnittstelle.


Welche Rolle spielt die Datei /etc/network/interfaces während des Bootvorgangs?

Sie enthält die Konfigurationen, die der Befehl ifup verwendet, um die entsprechenden Schnittstellen während des Bootvorgangs zu aktivieren.


Welcher Eintrag in /etc/network/interfaces konfiguriert die Schnittstelle eno1 so, dass sie ihre IP-Einstellungen über DHCP bezieht?

Die Zeile
iface eno1 inet dhcp


# Lösungen zu den offenen Übungen

Wie nutzen Sie den Befehl hostnamectl, um nur den statischen Hostnamen des lokalen Rechners in firewall zu ändern?

Mit der Option --static: hostnamectl --static set-hostname firewall.


Welche anderen Details als Hostnamen können Sie mit dem Befehl hostnamectl ändern?

hostnamectl kann auch das Standardsymbol für den lokalen Rechner, seinen Chassis-Typ, den Standort und die Einsatzumgebung festlegen.


Welcher Eintrag in /etc/hosts verbindet die beiden Namen firewall und router mit der IP 10.8.0.1?

Die Zeile
10.8.0.1 firewall router


Wie ändern Sie die Datei /etc/resolv.conf, um alle DNS-Anfragen an 1.1.1.1 zu senden?

Mit nameserver 1.1.1.1 als einzigem Nameserver-Eintrag.