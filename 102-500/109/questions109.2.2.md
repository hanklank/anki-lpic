Welche Netzwerk Schnittstellen ändert NetworkManager?

Nur nichtüberwachte Schnittstellen, das heißt er ignoriert die Einträge in /etc/network/interfaces um andere Tools nicht zu stören.


Wie geht NetworkManager mit Root Rechten um? Kann ein Nutzer ohne Root die Konfiguration bearbeiten?

Sein Dienst läuft mit Root Rechten. Da ein Nutzer ohne Root Rechte aber mit dem zugrundeliegenden Dienst kommunizieren darf (hier NM), führt dieser die angeforderten Aktionen durch.


Was verbirgt sich hinter nm-tray, nm-applet oder plasma-nm?

Ebenfalls existiert network-manager-gnome. Es sind Zubehör Anwendungen, die beispielweise ein Icon in der Leiste anzeigen.


Über welche beiden Client Dienste verfügt der NetworkManager? Welches ist umfangreicher?

nmcli und nmtui, nmcli ist umfangreicher.


Was geben diese Objekte für Informationen aus, wenn diese von nmcli aufgerufen werden?
general
networking
radio

general:
Allgemeiner Status und Betrieb von NetworkManager.
STATE      CONNECTIVITY  WIFI-HW  WIFI      WWAN-HW  WWAN    
connected  full          enabled  disabled  enabled  enabled

networking:
Allgemeine Netzwerkkontrolle.
enabled

radio:
NetworkManager-Funkschalter.
WIFI-HW  WIFI      WWAN-HW  WWAN    
enabled  disabled  enabled  enabled 


Was geben diese Objekte für Informationen aus, wenn diese von nmcli aufgerufen werden?
connection
device
agent

connection:
NetworkManager-Verbindungen.
NAME                    UUID                                  TYPE      DEVICE 
Steam Wired Connection  95401e96-9cc9-4e96-85de-ab2d39c83cd0  ethernet  eno1   
virbr0                  13e87e88-d6cb-49e5-974c-c5aae44ea707  bridge    virbr0 
Hotspot                 4c67fda3-6d80-4cb1-858e-3880f9cb371a  wifi      --     
WLAN_Privat             84077c68-b973-426c-9bcb-badeb618466c  wifi      -- 

device:
Von NetworkManager verwaltete Geräte.
DEVICE   TYPE      STATE                   CONNECTION             
eno1     ethernet  connected               Steam Wired Connection 
virbr0   bridge    connected (externally)  virbr0                 
wlp34s0  wifi      unavailable             --                     
lo       loopback  unmanaged               --  

agent:
NetworkManager Secret- oder Polkit-Agent.
nmcli successfully registered as a NetworkManager's secret agent.
Error: polkit agent failed: GDBus.Error:org.freedesktop.PolicyKit1.Error.Failed: An authentication agent already exists for the given subject


Was benötigt nmcli neben einem Objektargument (general, agent, device, ...)?

Ein Befehlsargument. status ist hierbei der Standard.


Wie sucht man mit nmcli nach den verfügbaren WLAN Netzen?
--
//- nmcli device wifi list
//- nmcli wifi search
//- nmcli device wifi scan
//- nmcli device list wifi 
//- nmcli device wlan list
--

- anmcli device wifi list


Wie verbinde ich mich mit nmcli mit einem WLAN Netzwerk?
--
//- nmcli device wifi connect Hypnotoad password MyPassword
//- nmcli device wlan connect Hypnotoad password MyPassword
//- nmcli device connect wifi Hypnotoad password MyPassword
//- nmcli device Hypnotoad wifi connect password MyPassword
//- nmcli device wlan enable Hypnotoad password MyPassword
--

- nmcli device wifi connect Hypnotoad password MyPassword


Wie nutze ich nmcli um mich mit dem gespeicherten Netzwerk wlo2 zu verbinden/trennen?
--
//- nmcli device (dis)connect wlo2
//- nmcli wlo2 device (dis)connect 
//- nmcli device wlo2 (dis)connect
//- nmcli device (dis/en)able wlo2
//- nmcli network (dis)connect wlo2
--

- nmcli device (dis)connect wlo2


Wie schalte ich mit nmcli den Funkadapter aus?
--
//- nmcli radio wifi off
//- nmcli radio wifi false
//- nmcli radio wlan off
//- nmcli wlan off
//- nmcli wlan false off
--

- nmcli radio wifi off


Wie heißt der Dienst zur Verwaltung Netzwerkkonnektivität unter systemd?

systemd-networkd


Wie heißt der Dienst zur Verwaltung lokalen Namensauflösung unter systemd?

systemd-resolved


Wo liegen die Dateien für systemd-networkd?
Netzwerkverzeichnis des Systems
Flüchtiges Netzwerkverzeichnis zur Laufzeit
Netzwerkverzeichnis der lokalen Verwaltung

/lib/systemd/network
/run/systemd/network
/etc/systemd/network
Wozu dienen die Dateien?

Netzwerkverzeichnis des Systems
Flüchtiges Netzwerkverzeichnis zur Laufzeit
Netzwerkverzeichnis der lokalen Verwaltung


Wie behandelt systemd-networkd die Priorität der Dateien in /lib, /run und /etc?

/etc haben die höchste Priorität, während Dateien in /run Vorrang vor Dateien gleichen Namens in /lib


Welcher ist der wichtigste Suffix für Dateien unter systemd-networkd?

.network für Netzwerkadressen und Routen


Wo legt man eine gehashte Datei wpa_supplicant-wlo1 mit dem Passwort des WLANs My Wifi an?
Welche Endung hat diese?
wpa_passphrase MyWifi > ???

/etc/wpa_supplicant/wpa_supplicant-wlo1.conf


Wie starte oder aktiviere ich beim Boot pa_supplicant@wlo1.service?
In welchem Ordner muss eine .network Datei vorhanden sein, die zu wl01 passt?

systemctl start wpa_supplicant@wlo1.service
systemctl enable wpa_supplicant@wlo1.service
In /etc/systemd/network/.


# Lösungen zu den offenen Übungen

Was bedeutet das Wort Portal in der Spalte CONNECTIVITY in der Ausgabe des Befehls nmcli general status?

Es bedeutet, dass zusätzliche Authentifizierungsschritte (in der Regel über den Webbrowser) erforderlich sind, um den Verbindungsprozess abzuschließen.


Wie nutzt ein normaler Benutzer in einem Konsolenterminal den Befehl nmcli, um sich mit dem drahtlosen Netzwerk MyWifi zu verbinden, das durch das Passwort MyPassword geschützt ist?

In einem reinen Textterminal lautet der Befehl:
$ nmcli device wifi connect MyWifi password MyPassword


Mit welchem Befehl schalten Sie den Wireless-Adapter ein, wenn er zuvor vom Betriebssystem deaktiviert wurde?

$ nmcli radio wifi on


In welchem Verzeichnis sollten benutzerdefinierte Konfigurationsdateien abgelegt werden, wenn systemd-networkd die Netzwerkschnittstellen verwaltet?

Im Netzwerkverzeichnis der lokalen Verwaltung: /etc/systemd/network.


# Lösungen zu den offenen Übungen

Wie führen Sie den Befehl nmcli aus, um eine nicht verwendete Verbindung namens Hotel Internet zu löschen?

$ nmcli connection delete "Hotel Internet"


NetworkManager scannt WLAN-Netzwerke in regelmäßigen Abständen, und der Befehl nmcli device wifi list listet nur die Zugangspunkte auf, die bei der letzten Suche gefunden wurden. Wie führen Sie den Befehl nmcli aus, um NetworkManager aufzufordern, alle verfügbaren Zugangspunkte sofort erneut zu scannen?

Der Root-Benutzer führt nmcli device wifi rescan aus, um NetworkManager zu veranlassen, die verfügbaren Zugangspunkte erneut zu scannen.


Welcher name-Eintrag sollte im [Match]-Abschnitt einer systemd-networkd-Konfigurationsdatei stehen, um alle Ethernetschnittstellen zu matchen?

Der Eintrag name=en*, da en das Präfix für Ethernetschnittstellen in Linux ist und systemd-networkd Shell-ähnliche Globs akzeptiert.


Wie führen Sie den Befehl wpa_passphrase aus, damit das als Argument angegebene Passwort verwendet und nicht von der Standardeingabe eingelesen wird?

Das Passwort sollte direkt nach der SSID angegeben werden, wie in wpa_passphrase MyWifi MyPassword.