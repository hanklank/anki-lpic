# Eigene Fragen

Wie legt man in der Shell eine lokale, shellspezifische Variable an?

<Variablenname>=<Variablenwert>


Welche Zeichen dürfen Variablennamen nur enthalten und womit und was muss beim ersten Zeichen eines Namens beachtet werden?

Buchstaben (a-z,A-Z), Zahlen (0-9) und Unterstriche (_). Er darf keine Zahl sein.


Welche Zeichen dürfen die Variablen selbst enthalten?

Variablen können beliebige alphanumerische Zeichen (a-z,A-Z,0-9) sowie die meisten anderen Zeichen (?,!,*,.,/ etc.) enthalten.


Wie setzt man die lokale Variable distro auf zorin 12.4?

$ distro="zorin 12.4"


Auf welche zwei Arten kann man eine Variable nur lesbar machen?

$ readonly reptile=tortoise
oder
$ readonly reptile


Wie kann man sich alle nur lesbaren Variablen in der aktuellen Shell anzeigen lassen?

readonly


Wie kann man sich alle Variablen anzeigen lassen?

set


Wie kann man das Setzen einer Variable rückgängig machen?

unset


Wie lautet die Konvention für lokale und globale Variablen?

lokal = test_var
global = SHELL


Wie füllt man eine Variable mit dem Inhalt einer anderen Variablen? (my_shell mit dem Inhalt von SHELL)

my_shell=$SHELL


Wie mache ich eine lokale Variable zu einer Umgebungsvariablen? Wie erzeuge ich Umgebungsvariablen?

export my_shell
export my_shell=$SHELL
Diese ist dann in Subshells verwendbar.


Wie wandle ich eine Umgebungsvariable wieder in eine lokale Variable um?

export -n my_shell


Wie liste ich alle Umgebungsvariable auf? (4 Möglichkeiten)

export
declare -x
env
printenv


Wie kann ich, abgesehen von echo, noch eine Variable ausgeben?

printenv my_var (kein $!)


Wie startet man eine neue Bash Shell ohne bereits gesetzte Variablen?

env -i bash


Wie funktioniert die initialisierung von Variablen in einer non-interactive, non-login Sitzung? (.bashrc, .profile, etc.)

Sie verwenden keine Startdateien. Lediglich der Inhalt der in der Variable BASH_ENV referenzierten Skripte werden eingelesen.


In welcher Variable wird der folgende Inhalt gespeichert? Eine Zahl (normalerweise 0), die auf den Bildschirm des Computers verweist.

DISPLAY


In welcher Variable wird der folgende Inhalt gespeichert?
ignorespace | Befehle, die mit einem Leerzeichen beginnen, werden nicht gespeichert.
ignoredups  | Ein Befehl, der mit dem vorherigen identisch ist, wird nicht gespeichert.
ignoreboth  | Befehle, die in eine der beiden vorherigen Kategorien fallen, werden nicht gespeichert.

HISTCONTROL


In welcher Variable wird der folgende Inhalt gespeichert? Dies bestimmt die Anzahl der Befehle, die während der Dauer der Shellsitzung im Speicher vorgehalten wird.

HISTSIZE


In welcher Variable wird der folgende Inhalt gespeichert? Dies bestimmt die Anzahl der Befehle, die sowohl zu Beginn als auch am Ende der Sitzung in HISTFILE gespeichert werden.

HISTFILESIZE


In welcher Variable wird der folgende Inhalt gespeichert? Der Name der Datei, die alle Befehle speichert, wie sie eingegeben werden. Standardmäßig befindet sich diese Datei unter ~/.bash_history.

HISTFILE


In welcher Variable wird der folgende Inhalt gespeichert? Diese Variable speichert den absoluten Pfad des Homeverzeichnisses des aktuellen Benutzers und wird gesetzt, wenn sich der Benutzer anmeldet.

HOME


In welcher Variable wird der folgende Inhalt gespeichert? Diese Variable speichert den TCP/IP-Namen des Host-Computers.

HOSTNAME


In welcher Variable wird der folgende Inhalt gespeichert? Speichert die Prozessorarchitektur des Host-Computers.

HOSTTYPE


In welcher Variable wird der folgende Inhalt gespeichert? Diese Variable speichert das Gebietsschema des Systems.

LANG


In welcher Variable wird der folgende Inhalt gespeichert? Diese Variable besteht aus einer durch Doppelpunkte getrennten Reihe von Verzeichnissen, in denen Shared Librabries von Programmen gemeinsam genutzt werden.

LD_LIBRARY_PATH


In welcher Variable wird der folgende Inhalt gespeichert? Diese Variable speichert die Datei, in der die Bash nach E-Mails sucht.

MAIL


In welcher Variable wird der folgende Inhalt gespeichert? Diese Variable speichert einen numerischen Wert, der in Sekunden das Intervall angibt, in dem die Bash nach neuen Mails sucht.

MAILCHECK


In welcher Variable wird der folgende Inhalt gespeichert? Diese Umgebungsvariable speichert die Liste der Verzeichnisse, in denen die Bash nach ausführbaren Dateien sucht, wenn sie aufgefordert wird, ein Programm auszuführen.

PATH


In welcher Variable wird der folgende Inhalt gespeichert? Diese Variable speichert den Wert der Basheingabeaufforderung (Prompt).

PS1


In welcher Variable wird der folgende Inhalt gespeichert? Diese Variable speichert den absoluten Pfad der aktuellen Shell.

SHELL


In welcher Variable wird der folgende Inhalt gespeichert? Hier wird der Name des aktuellen Benutzers gespeichert.

USER

# Lösungen zu den geführten Übungen

Betrachten Sie die Variablenzuweisung in der Spalte “Befehl(e)” und geben Sie an, ob die resultierende Variable “Lokal” oder “Global” ist:
Befehl(e) 	                    Lokal 	Global
debian=mother                   Ja      Nein
ubuntu=deb-based                Ja      Nein
mint=ubuntu-based; export mint  Nein    Ja


Betrachten Sie die Variablenzuweisung in der Spalte “Befehl(e)” und geben Sie an, ob die resultierende Variable “Lokal” oder “Global” ist:
export suse=rpm-based           Nein    Ja
zorin=ubuntu-based              Ja      Nein


Betrachten Sie den “Befehl” sowie dessen “Ausgabe” und nennen Sie deren Bedeutung:
Befehl 	            Ausgabe 	Bedeutung
echo $HISTCONTROL   ignoreboth      Sowohl doppelte Befehle als auch solche, die mit einem Leerzeichen beginnen, werden nicht in history gespeichert.
echo ~              /home/carol     Das HOME von carol ist /home/carol.
echo $DISPLAY       reptilium:0:2   Das System reptilium hat einen X-Server laufen, und wir verwenden den zweiten Bildschirm der Anzeige.


Betrachten Sie den “Befehl” sowie dessen “Ausgabe” und nennen Sie deren Bedeutung:
Befehl 	            Ausgabe 	Bedeutung
echo $MAILCHECK     60              Mail wird jede Minute geprüft.
echo $HISTFILE      /home/carol/.bash_history   history wird in /home/carol/.bash_history gespeichert.


In der Spalte “Falscher Befehl” sind Variablen falsch gesetzt. Geben Sie die fehlenden Informationen unter “Richtiger Befehl” und “Variablenreferenz” an, um die “Erwartete Ausgabe” zu erhalten:
Falscher Befehl 	        Richtiger Befehl 	            Variablenreferenz 	Erwartete Ausgabe
lizard =chameleon           lizard=chameleon                echo $lizard        chameleon
cool lizard=chameleon       cool_lizard=chameleon           echo $cool_lizard   chameleon
lizard=cha|me|leon          lizard="cha|me|leon" or         echo $lizard        cha|me|leon
                            lizard='cha|me|leon'


In der Spalte “Falscher Befehl” sind Variablen falsch gesetzt. Geben Sie die fehlenden Informationen unter “Richtiger Befehl” und “Variablenreferenz” an, um die “Erwartete Ausgabe” zu erhalten:
Falscher Befehl 	        Richtiger Befehl 	            Variablenreferenz 	Erwartete Ausgabe
lizard=/** chameleon **/    lizard="/** chameleon **/" or   echo "$lizard"      /** chamelon **/
                            lizard='/** chameleon **/'
win_path=C:\path\to\dir\    win_path=C:\\path\\to\\dir\\    echo $win_path      C:\path\to\dir\


Betrachten Sie den “Zweck” und schreiben Sie den entsprechenden Befehl:
Zweck 	                                                                                    Befehl
Setzen Sie die Sprache der aktuellen Shell auf Spanisch UTF-8 (es_ES.UTF-8).                LANG=es_ES.UTF-8
Ausgabe des Namens des aktuellen Arbeitsverzeichnisses.                                     echo $PWD or pwd


Betrachten Sie den “Zweck” und schreiben Sie den entsprechenden Befehl:
Zweck 	                                                                                    Befehl
Verweist auf die Umgebungsvariable, die die Informationen über ssh-Verbindungen speichert.  echo $SSH_CONNECTION
Setzen Sie PATH so, dass /home/carol/scripts als letztes Verzeichnis für die Suche nach     PATH=$PATH:/home/carol/scripts
ausführbaren Dateien einbezogen wird.


Betrachten Sie den “Zweck” und schreiben Sie den entsprechenden Befehl:
Zweck 	                                                                                    Befehl
Setzen Sie den Wert von my_path auf PATH.                                                   my_path=PATH
Setzen Sie den Wert von my_path auf den Wert von PATH.                                      my_path=$PATH


Erzeugen Sie eine lokale Variable namens mammal und weisen Sie ihr den Wert gnu zu: mammal=gnu
Erstellen Sie mit Hilfe der Variablensubstitution eine weitere lokale Variable namens var_sub mit dem entsprechenden Wert, so dass Sie, wenn Sie über echo $var_sub referenzieren, folgendes erhalten: The value of mammal is gnu:

var_sub="The value of mammal is $mammal:"


Machen Sie mammal zu einer Umgebungsvariablen:

export mammal


Suchen Sie nach der Umgebungsvariablen mammal mit set und grep:

set | grep mammal


Suchen Sie nach der Umgebungsvariablen mammal mit env und grep:

env | grep mammal


Erstellen Sie in zwei aufeinanderfolgenden Befehlen eine Umgebungsvariable namens BIRD, deren Wert penguin ist:

BIRD=penguin; export BIRD


Erstellen Sie in einem einzigen Befehl eine Umgebungsvariable namens NEW_BIRD, deren Wert yellow-eyed penguin ist:

export NEW_BIRD="yellow-eyed penguin"
or
export NEW_BIRD='yellow-eyed penguin'


Erstellen Sie als user2 ein Verzeichnis namens bin in Ihrem Homeverzeichnis: (3 Wege!)

mkdir ~/bin
mkdir /home/user2/bin
mkdir $HOME/bin


Geben Sie den Befehl ein, um das Verzeichnis ~/bin zu Ihrem PATH hinzuzufügen, damit er das erste Verzeichnis ist, in dem bash nach ausführbaren Dateien sucht:

PATH="$HOME/bin:$PATH"
PATH=~/bin:$PATH oder PATH=/home/user2/bin:$PATH sind ebenfalls korrekt.


Welches Stück Code — in Form einer if-Anweisung — fügen Sie in ~/.profile ein, um sicherzustellen, dass der Wert von PATH ($HOME/bin) über Neustarts hinweg unverändert bleibt?
if [ -d "path exists" ] ; then
...
fi

if [ -d "$HOME/bin" ] ; then
PATH="$HOME/bin:$PATH"
fi


# Lösungen zu den offenen Übungen
# not prepared for anki yet:

let: mehr als arithmetische Ausdrucksauswertung:

Suchen Sie in einer Manpage oder im Web nach let und seine Auswirkungen beim Setzen von Variablen und erstellen Sie eine neue lokale Variable namens my_val, deren Wert 10 ist — als Ergebnis der Addition von 5 und 5:

let "my_val = 5 + 5"
oder
let 'my_val = 5 + 5'


Erzeugen Sie nun eine weitere Variable namens your_val, deren Wert 5 ist — als Ergebnis der Division des Wertes von my_val durch 2:

let "your_val = $my_val / 2"
oder
let 'your_val = $my_val / 2'


Das Ergebnis eines Befehls in einer Variablen? Natürlich ist das möglich; man nennt es Befehlssubstitution. Recherchieren und studieren Sie die folgende Funktion namens music_info:

music_info(){
latest_music=`ls -l1t ~/Music | head -n 6`
echo -e "Your latest 5 music files:\n$latest_music"
}

Das Ergebnis des Befehls ls -l1t ~/Music | head -n 6 wird zum Wert der Variablen latest_music. Dann wird die Variable latest_music im Befehl echo referenziert (der die Gesamtzahl der vom Ordner Music belegten Bytes und die letzten fünf im Ordner Music gespeicherten Musikdateien ausgibt — eine pro Zeile).

Welcher der folgenden Begriffe ist ein gültiges Synonym für

latest_music=`ls -l1t ~/Music | head -n 6`

Es ist Option A:

latest_music=$(ls -l1t ~/Music| head -n 6)