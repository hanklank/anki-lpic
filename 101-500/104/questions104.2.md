# Lösungen zu den geführten Übungen

Wie prüfen Sie mittels du, wie viel Speicherplatz nur von den Dateien im aktuellen Verzeichnis benutzt wird?

Verwenden Sie zunächst die Parameter:
-S      um Unterverzeichnissen nicht mitzuberechnen
-d 0    um die Ausgabetiefe auf Null zu begrenzen, was Unterverzeichnisse ausschließt
-h      für eine Ausgabe in einem menschenlesbaren Format
$ du -S -h -d 0
oder
$ du -Shd 0

Listen Sie mit df Informationen für jedes ext4-Dateisystem auf, wobei die Ausgaben die Felder in der folgenden Reihenfolge enthalten: Gerät, Einhängepunkt, Gesamtzahl der Inodes, Anzahl der verfügbaren Inodes, Prozentsatz des freien Speicherplatzes. (source,target,itotal,iavail,pcent)

Sie können Dateisysteme mit der Option -t gefolgt vom Dateisystemnamen filtern. Für die benötigte Ausgabe benutzen Sie --output=source,target,itotal,iavail,pcent. Die Antwort lautet also: --output=source,target,itotal,iavail,pcent:
df -t ext4 --output=source,target,itotal,iavail,pcent

Wie lautet der Befehl, e2fsck auf /dev/sdc1 im nicht-interaktiven Modus auszuführen und dabei zu versuchen, die meisten Fehler automatisch zu beheben?


Der Parameter zur automatischen Fehlerbehebung ist -p. Die Antwort lautet also:
e2fsck -p /dev/sdc1

Angenommen, /dev/sdb1 ist ein ext2-Dateisystem. Wie können Sie seine Bezeichnung in UserData ändern?


tune2fs -L UserData /dev/sdb1

# Lösungen zu den offenen Übungen

Angenommen, Sie haben ein ext4-Dateisystem auf /dev/sda1 mit den folgenden Parametern, die Sie mit tune2fs ermittelt haben:
Mount count:              8
Maximum mount count:      -1
Was passiert beim nächsten Bootvorgang, wenn zuvor der Befehl tune2fs -c 9 /dev/sda1 ausgeführt wird?


Der Befehl setzt die maximale Mount-Zahl für das Dateisystem auf 9. Da die aktuelle Mount-Zahl bei 8 liegt, wird beim nächsten Systemstart eine Überprüfung des Dateisystems durchgeführt.

Betrachten Sie die folgende Ausgabe von du -h:
du -h
216K	./somedir/anotherdir
224K	./somedir
232K	.
Wie viel Platz wird nur von den Dateien im aktuellen Verzeichnis belegt? Wie könnten Sie den Befehl umschreiben, um diese Informationen deutlicher zu zeigen?


Von den insgesamt 232 K werden 224 K von dem Unterverzeichnis somedir und seinen Unterverzeichnissen verwendet. Wenn man diese abzieht, haben wir also 8 K, die von den Dateien im aktuellen Verzeichnis belegt werden. Diese Information kann durch Verwendung des Parameters -S, der die Verzeichnisse in der Aufzählung trennt, deutlicher dargestellt werden.

Was würde mit dem ext2-Dateisystem /dev/sdb1 passieren, wenn der unten stehende Befehl ausgeführt wird?
tune2fs -j /dev/sdb1 -J device=/dev/sdc1 -i 30d


Eine Journal wird zu /dev/sdb1 hinzugefügt, das damit in ext3 umgewandelt wird. Das Journal wird auf dem Gerät /dev/sdc1 gespeichert, und das Dateisystem wird alle 30 Tage überprüft.

Was ist der Unterschied zwischen den Parametern -T und -t für df?


-T Typ des FS anzeigen
-t ist ein Filter und zeigt in der Ausgabe nur Dateisysteme des angegebenen Typs an — alle anderen werden ausgeblendet.

# Eigene

Wie zeigt man sich den Inode einer Datei an?


ls -i

Wie sucht man nach der bestimmten Inode Zahl 585?


find . -inum 585

Wie zeigt man sich die gebrauchten und verfügbaren Inodes an?


df -i

Wie lässt man sich die benutze Größe von allen Dateien in einem lesbaren Format außer PDF Dateien nur in /usr anzeigen? Ähnlich zu ncdu?


du -h --max-depth=1 --exclude="*.pdf" /usr
du -h -d=1 --exclude="*.pdf" /usr (depth)

Wie führt man eine FS überprüfung auf /dev/sdb1 aus?


umount /dev/sdb1
fsck /dev/sdb1

Wie zeigt man sich Informationen über den Superblock einer Platte an?


sudo dump2fs -h /dev/sdb1
sudo tune2fs -l /dev/sdb1

Worin liegt der Unterschied zwischen du und df?


du arbeitet auf der Dateiebene. df auf der Dateisystemebene.
df zeigt an wo ein FS gemountet ist.