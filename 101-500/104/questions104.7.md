# Lösungen zu den geführten Übungen

Suchen Sie mit find ausschließlich im aktuellen Verzeichnis nach Dateien, die vom Benutzer beschreibbar sind (-writable), in den letzten 10 Tagen geändert wurden und größer als 4 GiB sind.


Hierfür benötigen Sie die Parameter -writable, -mtime und -size:
find . -writable -mtime -10 -size +4G

Suchen Sie mit locate alle Dateien, die in ihrem Namen die Muster report und entweder updated, update oder updating enthalten.


Da locate auf alle Muster passen muss, verwenden Sie die Option -A:
locate -A "report" "updat"

Wie können Sie herausfinden, wo die Manpage für ifconfig gespeichert ist?


Verwenden Sie den Parameter -m für whereis:
whereis -m ifconfig

Welche Variable muss zu /etc/updatedb.conf hinzugefügt werden, damit updatedb ntfs-Dateisysteme ignoriert?


Die Variable ist PRUNEFS=, gefolgt vom Dateisystemtyp: PRUNEFS=ntfs

Ein Systemadministrator möchte eine interne Festplatte (/dev/sdc1) einhängen. Unter welchem Verzeichnis soll diese Platte laut FHS gemountet werden?


In der Praxis kann die Festplatte überall eingehängt werden. Der FHS empfiehlt jedoch, dass temporäre Einhängungen unter /mnt erfolgen.

# Lösungen zu den offenen Übungen

Wenn locate verwendet wird, werden die Ergebnisse aus einer von updatedb erzeugten Datenbank bezogen. Diese Datenbank kann jedoch veraltet sein, wodurch locate Dateien anzeigt, die nicht mehr existieren. Wie kann man locate dazu bringen, nur existierende Dateien in seiner Ausgabe abzuzeigen?


Fügen Sie den Parameter -e zu locate hinzu, wie in locate -e PATTERN.

Finden Sie alle Dateien im aktuellen Verzeichnis oder deren Unterverzeichnissen bis zur 2 Ebenen tiefer, mit Ausnahme von eingehängten Dateisystemen, die das Muster Status oder statute in ihrem Namen enthalten.


Denken Sie daran, dass Sie für -maxdepth auch das aktuelle Verzeichnis berücksichtigen müssen, wir wollen also drei Ebenen (das Aktuelle plus 2 Ebenen tiefer):
find . -maxdepth 3  -mount -iname "*statu*"

Beschränkt auf die Suche von ext4-Dateisystemen, finden Sie alle Dateien unter /mnt, die mindestens Ausführungsrechte für die Gruppe haben, für den aktuellen Benutzer lesbar sind und bei denen in den letzten 2 Stunden ein beliebiges Attribut geändert wurde.

Verwenden Sie den Parameter -fstype wie beim Befehl mount, um die Suche auf bestimmte Dateisystemtypen zu beschränken. Eine Datei, die vom aktuellen Benutzer lesbar ist, hätte mindestens 4 an der ersten Stelle der Berechtigungen, und eine von der Gruppe ausführbare hätte mindestens den Wert 1 an der zweiten Stelle. Da wir uns nicht um die Berechtigungen für den Rest der Welt kümmern, können wir 0 für die dritte Stelle angeben. Verwenden Sie -cmin N, um die letzten Attributänderungen zu filtern, und denken Sie daran, dass N in Minuten angegeben wird. Also:
find /mnt -fstype ext4 -perm -410 -cmin -120

Suchen Sie leere Dateien, die vor mehr als 30 Tagen erstellt wurden und sich mindestens zwei Ebenen unterhalb des aktuellen Verzeichnisses befinden!


Der Parameter -mindepth N kann verwendet werden, um die Suche auf mindestens N Ebenen nach unten zu begrenzen, aber denken Sie daran, dass Sie das aktuelle Verzeichnis in die Zählung einbeziehen müssen. Verwenden Sie -empty, um nach leeren Dateien zu suchen, und -mtime N, um nach der Änderungszeit zu suchen. Also:
find . -empty -mtime +30 -mindepth 3

Nehmen wir an, dass die Benutzer carol und john Teil der Gruppe mkt sind. Suchen Sie im Homeverzeichnis von john alle Dateien, die auch von carol gelesen werden können.


In Anbetracht der Tatsache, dass beide Benutzer Mitglieder der gleichen Gruppe sind, brauchen wir mindestens ein r (4) für die Gruppenberechtigungen, und die anderen sind uns egal. Also:
find /home/john -perm -040

# Eigene

Wie sucht man nur bis Ebene N mit find und du? Wie wäre das für 3 Ordner tief?


find -maxdeth 4 und du -d 4

Wie unterscheiden sich -mmin und -mtime? Was bedeutet -amin, -cmin und -mmin?

-Xmin ist für Minuten und -Xtime für Tage.
-aXXX = access (Zugriff vor)
-cXXX = create (erstellt vor)
-mXXX = modify (verändert vor)

Was gibt which/type -a an?


Falls mehrere Dateien zum Command passen, werden alle ausgegeben.

Wie sucht man mit locate nach allen Dateien die .jpg oder .JPG heißen? Wie zählt man die Ergebnisse?


locate -i .jpg, zum zählen hängt man ein -c dran.

Wie führt man ein Update der locate Datenbank aus?


sudo updatedb

Wo liegt die Config der locate Datenbank?


/etc/updatedb.conf