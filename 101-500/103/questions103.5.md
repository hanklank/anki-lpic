# Lösungen zu den geführten Übungen

Starten Sie das Programm oneko.
1 Bewegen Sie den Mauszeiger, um zu sehen, wie die Katze ihn jagt. Halten Sie nun den entsprechenden Prozess an. Wie machen Sie das? Welche Ausgabe wird erzeugt?
2 Prüfen Sie, wie viele Jobs Sie derzeit haben. Was geben Sie ein? Welche Ausgabe erscheint?
3 Senden Sie den Prozess nun unter Angabe seiner Job-ID in den Hintergrund. Wie lautet die Ausgabe? Woran können Sie erkennen, dass der Job im Hintergrund läuft?
4 Die Katze bewegt sich wieder. Beenden Sie schließlich den Job unter Angabe seiner Job-ID. Was geben Sie ein?

1 Durch Drücken der Tastenkombination Strg+Z:
[1]+  suspended                 oneko
2 $ jobs
[1]+  suspended                 oneko
3 $ bg
[1]+  continued                 oneko
4 $ kill %1

Nennen Sie die PIDs aller Prozesse, die vom Apache HTTPD Webserver (apache2) erzeugt wurden, mittels zwei verschiedener Befehle:


$ pgrep apache2
oder
$ pidof apache2

Beenden Sie alle Prozesse von apache2 ohne Verwendung ihrer PIDs und mit zwei verschiedenen Befehlen:

$ pkill apache2
oder
$ killall apache2

Angenommen, Sie müssen alle Instanzen von apache2 beenden und haben keine Zeit, um herauszufinden, wie deren PIDs lauten. Wie wäre dies, unter Benutzung von`kill` mit dem Standardsignal SIGTERM in einem Einzeiler, erreichbar? (Nenne alle 4 Möglichkeiten!)

$ kill $(pgrep apache2)
$ kill `pgrep apache2`
oder
$ kill $(pidof apache2)
$ kill `pidof apache2`

Geben Sie den Befehl ps ein, um alle vom Benutzer Apache HTTPD Webserver gestarteten Prozesse anzuzeigen (www-data):


Unter Verwendung der UNIX-Syntax:
$ ps -u www-data

# Lösungen zu den offenen Übungen

Das Signal SIGHUP kann als eine Möglichkeit benutzt werden, bestimmte Daemons neu zu starten. Mit dem Apache HTTPD Webserver — zum Beispiel — beendet das Senden von SIGHUP an den Elternprozess (der durch init gestartet wurde) seine Kinder. Der Elternprozess liest jedoch seine Konfigurationsdateien erneut ein, öffnet die Logdateien erneut und erzeugt einen neuen Satz von Kindern. Führen Sie die folgenden Aufgaben aus:

Starten Sie den Apache2 Webserver:


$ sudo systemctl start apache2

Stellen Sie sicher, dass Sie die PID des übergeordneten Prozesses von Apache2 kennen:


$ ps aux | grep apache2
Der Elternprozess ist derjenige, der vom root-Benutzer gestartet wurde. In unserem Fall derjenige mit der PID 1653.

Starten Sie den Apache HTTPD Webserver neu, indem Sie ihm das Signal SIGHUP an den Elternprozess (PID 1653) senden:


kill -SIGHUP 1653

Geben Sie einen Befehl ein, der watch, ps und grep kombiniert, um Verbindungen von apache2 zu überwachen.

watch 'ps aux | grep apache2'
oder
watch "ps aux | grep apache2"

# Eigene Fragen

Was sind die Prefix Keys für Screen und Tmux?


Screen: STRG A
Tmux:   STRG B

Wie connected man einem vorher geöffneten screen?


screen -r

Wie verlässt man einen Screen oder Tmux?


Screen: STRG A -> D detach
Tmux:   STRG B -> D

Wie zeigt man sich unter Termux offene sessions an?


tmux ls

Wie teilt man unter Tmux das Fenster?


STRB B SHIFT 5

Wie wechselt man zwischen den Tmux Sessions? 5 Möglichkeiten


STRG B        ->      | Focus right window
STRG B        <-      | Focus left  window
STRG B        0       | Switch to window 0
STRG B        p       | previous
STRG B        n       | next

Wie erstellt man unter Tmux ein neues Fenster?


STRG B C

Mit welchen Befehlen kann man sich die Zeit seit dem Boot und RAM Statistiken anzeigen lassen?


uptime, free

Wie kann man einen prozess betrachten und das Output alle 2 Sekunden aktualisieren?


watch -n 2 date

Nenne die 4 verschiedenen Kill Signale und wie sie sich verhalten!


SIGINT  STRG+C  Kill it, but give it a chance to stop
SIGKILL         Kill it, last resort
SIGSTOP STRG+Z  Suspend a process
SIGTERM         Termination signal, which a process can still ignore, so it can clean up first

Wie pausiert man einen laufenden Command?


STRG Z

Wie setzt man einen pausierten Command im Vordergrund oder im Hintergrund fort?


fg
bg

Wie lässt man ein Skript explizit in der aktuellen Shell laufen? Wie und woran erkennt man, wie der default ist?


source test.sh/. test.sh und dies geschieht standardmäßig in einer neuen Shell, wenn #!/bin/bash in einem Skript steht

Wie legt man beim ausführen eines Commands (ping heise.de) fest, dass dieser im Hintergrund läuft?
Wie verhindert man dessen Output in der aktuellen Shell?


ping heise.de &
Mit nohup ping heise.de &

Wie kann man sich ansehen, welche Prozesse im Hintergrund laufen?


jobs

Wie erhält man die aktuell an die Shell attachten Prozesse von jedem User in einem user-friendly Format?


ps a            | Show all processes attached to a shell
ps  u           | Show all processes in an user friendly format
ps   x          | Show all processes by any user
ps aux          | Show all processes attached to a shell and by any user in an user friendly format

Wie erhält man die PID eines Prozesses und wie beendet man diesen dann?


pgrep
pidof
kill PID