# Lösungen zu den geführten Übungen

Betrachten Sie die folgende Ausgabe von top:
[...]
    PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
    1 root       20   0  171420  10668   7612 S   0,0   0,1   9:59.15 systemd
    2 root       20   0       0      0      0 S   0,0   0,0   0:02.76 kthreadd
    3 root       0  -20       0      0      0 I   0,0   0,0   0:00.00 rcu_gp
    4 root       0  -20       0      0      0 I   0,0   0,0   0:00.00 rcu_par_gp
    8 root       0  -20       0      0      0 I   0,0   0,0   0:00.00 mm_percpu_wq
    9 root       20   0       0      0      0 S   0,0   0,0   0:49.06 ksoftirqd/0
    10 root      20   0       0      0      0 I   0,0   0,0  18:24.20 rcu_sched
    11 root      20   0       0      0      0 I   0,0   0,0   0:00.00 rcu_bh
    12 root      rt   0       0      0      0 S   0,0   0,0   0:08.17 migration/0
    14 root      20   0       0      0      0 S   0,0   0,0   0:00.00 cpuhp/0
    15 root      20   0       0      0      0 S   0,0   0,0   0:00.00 cpuhp/1
    16 root      rt   0       0      0      0 S   0,0   0,0   0:11.79 migration/1
    17 root      20   0       0      0      0 S   0,0   0,0   0:26.01 ksoftirqd/1
Welche PIDs haben Echtzeitpriorität?


Die PIDs 12 und 16.

Betrachten Sie die folgende Auflistung von ps -el:
F S   UID   PID  PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
4 S     0     1     0  0  80   0 - 42855 -      ?        00:09:59 systemd
1 S     0     2     0  0  80   0 -     0 -      ?        00:00:02 kthreadd
1 I     0     3     2  0  60 -20 -     0 -      ?        00:00:00 rcu_gp
1 S     0     9     2  0  80   0 -     0 -      ?        00:00:49 ksoftirqd/0
1 I     0    10     2  0  80   0 -     0 -      ?        00:18:26 rcu_sched
1 I     0    11     2  0  80   0 -     0 -      ?        00:00:00 rcu_bh
1 S     0    12     2  0 -40   - -     0 -      ?        00:00:08 migration/0
1 S     0    14     2  0  80   0 -     0 -      ?        00:00:00 cpuhp/0
5 S     0    15     2  0  80   0 -     0 -      ?        00:00:00 cpuhp/1
Welche PID hat die höchste Priorität?


PID 12.

Nach dem Versuch, einen Prozess mit renice anzupassen, tritt der folgende Fehler auf:
renice -10 21704
renice: failed to set priority for 21704 (process ID): Permission denied
Was ist die wahrscheinliche Ursache für den Fehler?


Nur der Benutzer root kann den nice-Wert unter Null senken.

# Eigene Fragen

Von wo bis wo reichen die Nice Werte und was sagt ein niedriger/hoher Nice Wert aus?


Von -20 bis 19,
niedrig: wenig nett, hohe Priortität
hoch:    sehr nett, niedrige Priortität
        da nette Prozesse anderen Vorrang gewähren

Wie vergibt man einen nice Wert an einen Prozess beim ausführen? Was ist der Standard und wie vergibt man z.B. -15?


nice, 10 default,
nice -n -15
nice --15


Wie vergibt man einem Prozess nach der Ausführung eine andere Nettigkeit?


renice -15 -p PID

Welche Prioritätswerte haben Echtzeitprozesse und Statische Prozesse?


Echtzeitprozesse      0-99
Statische Prozess     100-139

Wie kennzeichnet top Echtzeitprozesse?


rt real time

Wie geben top und ps jeweils die Prozess Prioritäten an?


top     subtrahiert alle um 100
ps      subtrahiert alle um 40

Welche Priotität gewinnt? Hohe oder niedrige?


niedrig gewinnt.