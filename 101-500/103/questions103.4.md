# Lösungen zu den geführten Übungen

Zusätzlich zu Textdateien kann der Befehl cat auch mit binären Daten arbeiten, wie z.B. den Inhalt eines Blockgerätes in eine Datei senden. Wie kann cat mittels Umleitung den Inhalt vom Gerät /dev/sdc an die Datei sdc.img im aktuellen Verzeichnis senden?


cat /dev/sdc > sdc.img

Wie lautet der Name des Standardkanals, der durch den Befehl date 1> now.txt umgeleitet wird?


Standardausgabe oder stdout

Nach dem Versuch eine Datei mit Hilfe der Umleitung zu überschreiben, erhält ein Benutzer eine Fehlermeldung mit der Information, dass die Option noclobber aktiviert ist. Wie kann die Option noclobber für die aktuelle Sitzung deaktiviert werden?


set +C oder set +o noclobber

Wie lautet das Ergebnis des Befehls cat <<.>/dev/stdout?


Bash geht in den Heredoc-Eingabemodus und verlässt diesen, sobald ein Punkt in einer Zeile erscheint. Der eingegebene Text wird anschließend nach stdout umgeleitet (auf dem Bildschirm angezeigt).

# Lösungen zu den offenen Übungen

Der Befehl cat /proc/cpu_info zeigt eine Fehlermeldung, weil /proc/cpu_info nicht vorhanden ist. Wohin leitet der Befehl cat /proc/cpu_info 2>1 die Fehlermeldung um?


Zu einer Datei namens 1 im aktuellen Verzeichnis.

Wird es immer noch möglich sein, an /dev/null gesendete Inhalte zu verwerfen, wenn die Option noclobber für die aktuelle Shellsitzung aktiviert ist?


Ja, /dev/null ist eine spezielle Datei, die nicht von noclobber betroffen ist.

Wie könnte der Inhalt der Variable $USER ohne die Verwendung von echo auf stdin des Befehls sha1sum umgeleitet werden?


sha1sum <<<$USER

Der Linux-Kernel behält symbolische Links in /proc/PID/fd/ zu jeder Datei, die von einem Prozess geöffnet ist, wobei PID die Identifikationsnummer des entsprechenden Prozesses darstellt. Wie könnte der Systemadministrator dieses Verzeichnis benutzen, um den Ort der von nginx geöffneten Protokolldateien zu überprüfen, angenommen seine PID lautet 1234?


Durch den Befehl ls -l /proc/1234/fd, der die Ziele jedes symbolischen Links im Verzeichnis anzeigt.

Es ist möglich, arithmetische Berechnungen nur mit den in der Shell eingebauten Befehlen durchzuführen, aber Fließkommaberechnungen erfordern spezielle Programme wie bc (basic calculator). Mit bc ist es sogar möglich durch den Parameter scale die Anzahl der Nachkommastellen zu definieren. Allerdings akzeptiert bc Operationen nur über seine Standardeingabe, die normalerweise im interaktiven Modus eingegeben wird. Wie kann die Fließkommaoperation scale=6; 1/3 unter Verwendung eines Here-Strings an die Standardeingabe von bc geschickt werden?


bc <<<"scale=6; 1/3"


# Lösungen zu den geführten Übungen

Es ist üblich das Ausführungsdatum von Aktionen zu speichern, die von automatisierten Skripten ausgeführt werden. Der Befehl date +%Y-%m-%d zeigt das aktuelle Datum im Format Jahr-Monat-Tag. Wie kann die Ausgabe eines solchen Befehls mittels Befehlssubstitution in einer Shellvariablen namens TODAY gespeichert werden?


TODAY=`date +%Y-%m-%d` oder TODAY=$(date +%Y-%m-%d)

Wie kann der Inhalt der Variablen TODAY mittels des Befehls echo an die Standardeingabe des Befehls sed s/-/./g übergeben werden?


$ echo $TODAY | sed s/-/./g

Wie könnte die Ausgabe des Befehls date +%Y-%m-%d als Here-String verwendet werden, um sed s/-/./g zu instruieren?


$ sed s/-/./g <<< `date +%Y-%m-%d`
oder
$ sed s/-/./g <<< $(date +%Y-%m-%d)

Der Befehl convert image.jpeg -resize 25% small/image.jpeg erzeugt eine kleinere Version von image.jpeg und legt das resultierende Bild in einer gleichnamigen Datei des Unterverzeichnis small ab. Wie ist es mit xargs möglich, für jedes in der Datei filelist.txt aufgeführte Bild den gleichen Befehl auszuführen?

$ xargs -I IMG convert IMG -resize 25% small/IMG < filelist.txt
oder
$ cat filelist.txt | xargs -I IMG convert IMG -resize 25% small/IMG

# Lösungen zu den offenen Übungen

Eine einfache Sicherungsroutine erstellt periodisch ein Abbild der Partition /dev/sda1 mittels dd < /dev/sda1 > sda1.img. Um zukünftige Datenintegritätsprüfungen durchzuführen, erzeugt die Routine auch einen SHA1-Hash der Datei mittels sha1sum < sda1.img > sda1.sha1. Wie würden diese beiden Befehle durch Hinzufügen von Pipes und dem Befehl tee zu einem Einzigen kombiniert werden?


dd < /dev/sda1 | tee sda1.img | sha1sum > sda1.sha1

Anstatt eine neue Remote-Shell-Sitzung zu eröffnen, kann der Befehl ssh einfach einen Befehl ausführen, der als sein Argument angegeben ist: ssh user@storage "remote command". Da ssh auch erlaubt die Standardausgabe eines lokalen Programms auf die Standardeingabe des entfernten Programms umzuleiten, wie würde das cat eine lokale Datei mit dem Namen etc.tar.gz über die Pipe an /srv/backup/etc.tar.gz unter user@storage durch ssh weiterleiten?


cat etc.tar.xz | ssh user@storage "cat > /srv/backup/etc.tar.xz"
oder
ssh user@storage "cat > /srv/backup/etc.tar.xz" < etc.tar.xz

# Eigene Fragen

Was bewirkt noclobber und wie aktiviert man es?


set -C oder set -o noclobber Es bewirkt, dass ein Fehler ausgegeben wird wenn man eine Datei überschreiben möchte, die bereits existiert.

Nenne alle Kommunikationskanäle und deren Dateideskriptoren!


STDIN  Standardeingabe 0
SDTOUT Standardausgabe 1
SDTERR Standarderror   2

Wie kann man den Error des Befehls cat eins abfangen und in zwei umleiten, falls die Datei nicht existiert?


cat eins 1> zwei 2>&1

Wie fügt man den Output an eine Datei an anstatt diese zu Überschreiben?


> überschreiben, >> anfügen

Wie kann man das Output des Befehls ls -1 hinter einer Pipe in einem weiteren Befehl (rm) verwenden?


ls -1 | xargs rm

Erläutere den Unterschied zwischen Here Dokument und Here String und wie man diese angibt!


>> EOF      Manuelle Eingabe bis EOF
>>> Here    Eine Zeile wird nach STDIN umgeleitet

Wie lässt man sich zusätzlich zum Umleiten | den Inhalt anzeigen?


| tee

Wie kann man den Inhalt einer Datei an SDTIN eines weiteren Befehls weiterleiten? Wie macht man dies sonst?


Eines Befehls: <
Sonst z.B. durch Here String: <<<