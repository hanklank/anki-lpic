# Lösungen zu den geführten Übungen

Mit welcher Taste kann man Text einrücken/das Einrücken Rückgängig machen?


<</>>

Eine ganze Zeile kann durch Drücken von V im normalen Modus in vi ausgewählt werden. Das abschließende Zeilenumbruchzeichen ist jedoch ebenfalls enthalten. Welche Tasten sollten im normalen Modus gedrückt werden, um vom Startzeichen bis zum Zeilenumbruch, aber nicht einschließlich des Zeilenumbruchzeichens, auszuwählen?


Die Tasten 0v$h, d.h. 0 (“zum Zeilenanfang springen”), v (“Zeichenauswahl starten”), $ (“zum Zeilenende springen”) und h (“eine Position zurückgehen”).
Alternativ ist auch ^v$h möglich.

Wie sollte vi in der Kommandozeile ausgeführt werden, um ~/.bash_profile zu öffnen und direkt zur letzten Zeile zu springen?


Der Befehl vi + ~/.bash_profile öffnet die Datei und setzt den Cursor auf die letzte Zeile.

Welche Tasten sollten im Normalmodus in vi gedrückt werden, um Zeichen von der aktuellen Cursorposition bis zum nächsten Punktzeichen zu löschen?


Die Tasten dt., d.h. d (“Löschung starten”), t (“zum folgenden Zeichen springen”) und . (Punktzeichen).

# Lösungen zu den offenen Übungen

Eine vi-Sitzung wurde durch einen unerwarteten Stromausfall unterbrochen. Beim erneuten Öffnen der Datei fragt vi den Benutzer, ob er die Auslagerungsdatei (eine von vi automatisch erstellte Kopie) wiederherstellen möchte. Was sollte der Benutzer tun, um die Auslagerungsdatei zu verwerfen?


Durch Drücken von d delete, wenn Sie durch vi aufgefordert werden.

Wie führt man im Edit Mode eine globale Suche durch und ersetzt die gefundenen Zeichen?


:s/REGEX/TEXT/g

Welche 2 Möglichkeiten gibt es um die aktuell bearbeitete Datei zu schließen und zu speichern?


:wq und ZZ

Wie kopiert man eine Zeile und wie fügt man sie vor und nach der aktuell ausgewählten ein?


y yank
p print below
P print before

Wie springt man in Zeile 3, ans Ende der Zeile und an den Anfang der Zeile?


:3, $, ^/0